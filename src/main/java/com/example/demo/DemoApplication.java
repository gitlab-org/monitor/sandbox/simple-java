package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.ModelMap;
import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@RestController
public class DemoApplication {

	String style = "<style type='text/css' media='screen'>" + "body { background-color: orange; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }" + "</style>";

	@GetMapping("/")
	String home() {
        // TODO - Personalize this message!
        String message = "Simple Java is here!";
        System.out.println("handling GET");        
        String body =
                "<body>" +
                message +
                "<br>" +
                "<form action=\"/\" method=\"post\" >" +
				"<input type=\"submit\" value=\"Increase MEM\">" +
				"</form>" +
                "</body>";

        return style + body;
	}

	@PostMapping("/")
    public String javaScriptPost(ModelMap model, HttpServletRequest request) {
        System.out.println("handling POST");
	
		int numOfStrs = 20000000; // this number of strings will add about 436 MB of memory consumption
	
		String[] largeStrArray;
	
		largeStrArray = new String[numOfStrs];
	
		for (int i =0; i< numOfStrs; i++) {
			largeStrArray[i] = new String("0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
		}

		String message = "Memory consumption has been increased";

		String body = "<body>" + message + "<body>";

		return(style + body);
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
